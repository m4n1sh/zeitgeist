/* tracker.vala
 *
 * Copyright © 2013 Manish Sinha <manishsinha@ubuntu.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace Zeitgeist
{
    /*[DBus (name = "org.freedesktop.Tracker1.Miner.Files.Index")]
    interface TrackerFilesMinerInterface: Object
    {
        public abstract void index_file (string file_uri)
            throws Error;
        public abstract void reindex_mime_types(string[] mime_types)
            throws Error;	
    }*/
    
    public class TrackerExtension: Extension
    {
        //private 
        
        TrackerExtension ()
        {
            Object ();
        }
        
        construct
        {
            try
            {
                /*TrackerFilesMinerInterface miner = Bus.get_proxy_sync (BusType.SESSION, 
                        "org.freedesktop.Tracker1.Miner.Files.Index", 
                        "/org/freedesktop/Tracker1/Miner/Files/Index");*/
            }
            catch (Error err)
            {
                warning ("%s", err.message);
            }
            //warning ("Trying the constructor\n");
        }
        
        public override void post_insert_events (GenericArray<Event?> events,
            BusName? sender)
        {
            for (int i = 0; i < events.length; ++i)
            {
                Event e = events[i];
                string interpretation = e.interpretation;
                if(interpretation == Zeitgeist.ZG.MODIFY_EVENT || interpretation == Zeitgeist.ZG.CREATE_EVENT)
                {
                    for(int j = 0; j < e.num_subjects() ; j++)
                    {
                        Subject subj = e.get_subject(j);
                        if(subj.uri.has_prefix("file://"))
                        {
                            string uri = Uri.unescape_string(subj.uri);
                            //warning ("URI: %s\n", uri);
                            
                            string[] spawn_args = {"tracker-control", "--index-file", uri};
                            string[] spawn_env = Environ.get ();
                    		Pid child_pid;
                    		
                    		try {
                    		    Process.spawn_async (null,
			                        spawn_args,
			                        spawn_env,
			                        SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
			                        null,
			                        out child_pid);
			                    }
			                catch (SpawnError e) {
		                        warning ("Error: %s\n", e.message);
		                    }

		                    ChildWatch.add (child_pid, (pid, status) => {
			                    // Triggered when the child indicated by child_pid exits
			                    Process.close_pid (pid);
		                    });
                        }
                    }
                }
            }
        }
    }
    
    [ModuleInit]
#if BUILTIN_EXTENSIONS
    public static Type tracker_extension_init (TypeModule module)
    {
#else
    public static Type extension_register (TypeModule module)
    {
#endif
        return typeof (TrackerExtension);
    }
}
